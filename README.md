# SystemRDL to NaturalDocs

[[_TOC_]]

## Description

It generates registers memory map documentation from [SystemRDL][] input files to [NaturalDocs][] output.

[SystemRDL]: https://www.accellera.org/downloads/standards/systemrdl
[NaturalDocs]: https://www.naturaldocs.org
