# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import json
import pathlib
import subprocess

class Bitfield:
    def __init__(self):
        self._vspace     = 80
        self._hspace     = 640
        self._lanes      = 1
        self._bits       = 32
        self._fontsize   = 14
        self._fontfamily = 'sans-serif'
        self._fontweight = 'normal'
        self._compact    = False
        self._hflip      = False
        self._vflip      = False
        self._uneven     = False
        self._trim       = 0

    def __call__(self, register, destination) -> None:
        destination = pathlib.Path(destination).resolve()
        source = destination.with_suffix('.json')
        vspace = self._vspace
        bits = self._bits

        if not isinstance(register, list) and not isinstance(register, dict):
            bits = vars(register).get('width', self._bits)

        model = []
        config = []

        offset = int(0)

        for field in register:
            if not isinstance(field, dict):
                field = vars(field)

            field_width  = int(field.get('width',  1))
            field_offset = int(field.get('offset', 0))

            if offset < field_offset:
                model.append({'bits': field_offset - offset})
                offset = field_offset

            model.append({
                'name': field.get('name'),
                'bits': field_width,
                'attr': int(field.get('reset', 0)),
                'type': field.get('type'),
            })

            offset = field_offset + field_width

        if offset < bits:
            model.append({'bits': bits - offset})

        if source.is_file() and source.stat().st_size:
            with source.open('r', encoding='utf-8') as file:
                config = json.load(file)
        else:
            if not source.parent.exists():
                source.parent.mkdir(0o755, True, True)

            with source.open('w', encoding='utf-8') as file:
                json.dump(model, file)

            config = model

        if destination.is_file() and destination.stat().st_size and model == config:
            return

        arguments = (
            'bitfield',
            '--input',      str(source),
            '--bits',       str(bits),
            '--vspace',     str(vspace),
            '--hspace',     str(self._hspace),
            '--lanes',      str(self._lanes),
            '--fontsize',   str(self._fontsize),
            '--fontfamily', str(self._fontfamily),
            '--fontweight', str(self._fontweight),
            '--compact',    str(self._compact).lower(),
            '--hflip',      str(self._hflip).lower(),
            '--vflip',      str(self._vflip).lower(),
            '--uneven',     str(self._uneven).lower(),
            '--trim',       str(self._trim)
        )

        with subprocess.Popen(arguments, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as process:
            process.wait()

            with destination.open('w', encoding='utf-8') as file:
                file.write(process.stdout.read().decode('utf-8'))

