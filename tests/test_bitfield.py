# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import uuid
import json
import pathlib
import unittest
import tempfile
import rdl2nd.bitfield
import multiprocessing.pool

class TestBitfield(unittest.TestCase):
    def test_pool_generate(self):
        with tempfile.TemporaryDirectory() as directory:
            arguments = [
                (
                    [
                        {'name': 'field1', 'width': 3, 'offset': 1},
                        {'name': 'field2', 'width': 1, 'offset': 6},
                        {'name': 'field3', 'width': 8, 'offset': 20, 'reset': 5},
                    ],
                    pathlib.Path(directory) / (str(uuid.uuid4()) + '.svg'),
                ),
            ]

            with multiprocessing.pool.Pool() as pool:
                pool.starmap(rdl2nd.Bitfield(), arguments)

            expected = [
                [
                    {'bits': 1},
                    {'name': 'field1', 'bits': 3, 'attr': 0, 'type': None},
                    {'bits': 2},
                    {'name': 'field2', 'bits': 1, 'attr': 0, 'type': None},
                    {'bits': 13},
                    {'name': 'field3', 'bits': 8, 'attr': 0, 'type': None, 'attr': 5},
                    {'bits': 4},
                ],
            ]

            for index, argument in enumerate(arguments):
                self.assertTrue(argument[1].is_file())
                self.assertTrue(argument[1].stat().st_size != 0) # not empty
                self.assertTrue(argument[1].with_suffix('.json').is_file())
                self.assertTrue(argument[1].with_suffix('.json').stat().st_size != 0) # not empty

                with argument[1].with_suffix('.json').open('r', encoding='utf-8') as file:
                    self.assertEqual(json.load(file), expected[index])

if __name__ == '__main__':
    unittest.main()
