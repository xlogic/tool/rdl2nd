# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import pathlib

sys.path.insert(1, str(pathlib.Path(*pathlib.Path(__file__).resolve().parts[:-2]) / 'lib'))
